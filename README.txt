Collaborative-Whiteboard
========================

Table of Contents:
1. Overview
2. How to Run the Server
3. How to Run the Client
4. How to Use the Canvas
5. How to Use the Palette

1 OVERVIEW
This program is a collaborative whiteboard where multiple users can connect and draw on multiple canvases.  
Supports freehand drawing, erasing, stroke width, stroke color, paint fill, lines, and shapes.  

2 SERVER
To start the server, use the command "WhiteboardServer [-boards BOARDS] [-host HOST] [-port PORT]" which allows you to specify the HOST hostname/IP address and the PORT port the server will be listening on as well as the BOARDS number of boards the server will store. 
In Eclipse, the server can also be started by running WhiteboardServer.java in the server package.

3 CLIENT
To start the client, use the command "WhiteboardClient [-host HOST] [-port PORT]" which allows you to specify the HOST hostname/IP address and PORT port of the server the client should connect to.
In Eclipse, the client can also be started by running WhiteboardClient.java in the client package. Note that the client can only run after the server has been run.

4 CANVAS
Click anywhere on the canvas panel and drag the mouse to draw (freehand/line/rectangle/oval) or erase.  

5 PALETTE
The user can select (by clicking to toggle) the following modes: Draw, Erase, Fill, Rectangle, and Oval.  
The user can use the stroke slider to change the width of the stroke or open a color-selector popup to change the stroke color.  
The user can enter a board number into the textfield next to "Add/Set Canvas Name: " and press enter to change the board.  