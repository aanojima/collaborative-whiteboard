package ui;

/**
 * Mode represents the type of drawing Palette is selected to make 
 */
public enum Mode {
    DRAW, // freehand drawing
    ERASE, // erasing
    FILL, // filling color
    LINE, // straight line
    RECT, // rectangle
    OVAL // oval
}