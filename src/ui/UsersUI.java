package ui;

import java.util.ArrayList;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

/** TESTING STRATEGY
 * 
 *  	1. UserUI displays the users currently connected to the server
 *  	2. addUser(user) adds user to the table
 *  	3. removeUser(user) removes user from the table
 */

/** THREAD-SAFETY ARGUMENT
 * 		-all fields are final 
 *		-the only mutator methods, addUser and removeUser, are synchronized
 */

/**
 *  SPECIFICATIONS
 *  
 *  UsersUI contains a table that displays all the users currently connected to the server.
 *  Users and added/removed from the table when they connect/disconnect
 *  
 */

public class UsersUI extends JPanel {

    private static final long serialVersionUID = 1L;
    
    private final ArrayList<String> users;
    private final JLabel listTitle;
    private final JTable list;
    private final DefaultTableModel model;
    
    /**
     * 
     * @param width specifies width of UI in pixels
     * @param height specifies height of UI in pixels
     * @param initialUsers is list of usernames for users already on the server
     */
    public UsersUI(int width, int height, String[] initialUsers){
        
        this.setSize(width, height);
        
        model = new DefaultTableModel(){

            private static final long serialVersionUID = 5598221578072596831L;

            @Override
            public boolean isCellEditable(int row, int column) {
               //all cells false
               return false;
            }
        };
        model.addColumn("Users");
        listTitle = new JLabel("Available Users: ");
        listTitle.setName("listTitle");
        list = new JTable(model);
        list.setName("list");
        users = new ArrayList<String>();
        if (initialUsers.length > 0){
            for (String user : initialUsers){
                addUser(user);
            }
        }
        
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true); 
        
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addComponent(listTitle, width, width, width)
                .addComponent(list, width, width, width));
        
        layout.setVerticalGroup(
            layout.createSequentialGroup()
                .addComponent(listTitle, 20, 20, 20)
                .addComponent(list, height-20, height - 20, height - 20));
        
    }
    
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            public void run(){
                JFrame window = new JFrame();
                UsersUI main = new UsersUI(100, 400, new String[0]);
                window.setContentPane(main);
                window.setVisible(true);
                main.addUser(null);
            }
        });
    }
    
    /**
     * Adds users to the table
     * @param user is username of a newly connected user
     */
    public synchronized void addUser(String user){
        if (user == null || user.matches("")){
            return;
        }
        users.add(user);
        model.addRow(new String[] {user});
    }
    
    /**
     * Removes disconnected user from the table
     * @param user is username of newly disconnected user
     */
    public synchronized void removeUser(String user){
        if (user == null || user.matches("")){
            return;
        }
        int row = users.indexOf(user);
        users.remove(user);
        model.removeRow(row);
    }

}
