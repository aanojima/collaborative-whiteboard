package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * TESTING STRATEGY:
 * 
 * 1. Each JComponent is displayed properly
 * 	(draw, fill, erase, rect, oval, line; strokeWidthSlider, colorChooser, changeWhiteBoard)
 * 
 * 2. Listeners work properly
 * 		-clicking draw, fill, erase, oval, or line should change the mode
 * 		-clicking colorChooser brings up colorChooser dialog and choosing a new color changes the
 * 			current color of palette
 * 		-dragging strokeWidthSlider changes the strokeWidth of palette
 * 		-entering an integer between 0 and maxBoards in the changeWhiteboard textbox and pressing enter
 * 			changes the current canvas.
 */

/** THREAD-SAFETY ARGUMENT:
 * 
 * 	1. A Palette object is associated with one client and cannot be accessed by other clients
 * 	2. The only variables that can accessed outside Palette are color, strokeWidth, and mode
 * 		but getColor(), getStrokeWidth(), getMode(), and setMode() are all synchronized methods,
 * 		so these variables are locked whenever they are called
 */

/** SPECIFICATIONS
 * 
 * Palette represents the window containing editing tools with which the user can edit the canvas.
 * Each Palette object is associated with a Canvas object, and the two together is associated with
 * a client.  
 */

public class Palette extends JPanel {

    private static final long serialVersionUID = 1L;
    
    private final JButton changeBoardButton;
    private final JTextField changeBoardTextField;
    
    private final ButtonGroup modeButtonGroup;
    private final JToggleButton drawToggle;
    private final JToggleButton eraseToggle;
    private final JToggleButton fillToggle;
    private final JToggleButton lineToggle;
    private final JToggleButton rectToggle;
    private final JToggleButton ovalToggle;
    
    private final JLabel strokeWidthLabel;
    private final JSlider strokeWidthSlider;
    private final JButton chooseColorButton;

    private final JLabel whiteboardNameLabel;
    private final JLabel changeWhiteboardLabel;
    private final JTextField changeWhiteboard;
    
    private Mode mode;
    private int strokeWidth;
    private Color color;
    
    /**
     * Constructor of Palette object. Instantiates all JComponents (buttons, labels, etc.)
     * that will be displayed on the panel.
     * @param width is a non-negative integer
     * @param height is a non-negative integer
     * width and height specify the size of the palette window
     */
    public Palette(int width, int height) {
        
        this.setSize(new Dimension(width, height));
        
        this.mode = Mode.DRAW;
        this.strokeWidth = 1;
        this.color = Color.BLACK;
        
        changeBoardButton = new JButton("Change Board");
        changeBoardButton.setName("Change Board");
        
        changeBoardTextField = new JTextField();
        changeBoardTextField.setName("Change Board ID");
        
        modeButtonGroup = new ButtonGroup();
        
        //add JComponents to the Panel
        ImageIcon drawIcon = new ImageIcon("resources/images/draw.jpg", "");
        Image drawImage = drawIcon.getImage();
        drawImage = drawImage.getScaledInstance(15, 15,  java.awt.Image.SCALE_SMOOTH );    
        drawIcon = new ImageIcon(drawImage); 
        drawToggle = new JToggleButton("Draw", drawIcon);
        drawToggle.setName("Draw");  
        modeButtonGroup.add(drawToggle);
        
        ImageIcon eraseIcon = new ImageIcon("resources/images/eraser.png", "");
        Image eraseImage = eraseIcon.getImage();
        eraseImage = eraseImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        eraseIcon = new ImageIcon(eraseImage);
        eraseToggle = new JToggleButton("Erase", eraseIcon);
        eraseToggle.setName("Erase");
        modeButtonGroup.add(eraseToggle);
        
        ImageIcon fillIcon = new ImageIcon("resources/images/fill.png", "");
        Image fillImage = fillIcon.getImage();
        fillImage = fillImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        fillIcon = new ImageIcon(fillImage);
        fillToggle = new JToggleButton("Fill", fillIcon);
        fillToggle.setName("Fill");
        modeButtonGroup.add(fillToggle);
        
        ImageIcon lineIcon = new ImageIcon("resources/images/line.png", "");
        Image lineImage = lineIcon.getImage();
        lineImage = lineImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        lineIcon = new ImageIcon(lineImage);
        lineToggle = new JToggleButton("Line", lineIcon);
        lineToggle.setName("Line");
        modeButtonGroup.add(lineToggle);
        
        ImageIcon rectIcon = new ImageIcon("resources/images/rect.jpeg", "");
        Image rectImage = rectIcon.getImage();
        rectImage = rectImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        rectIcon = new ImageIcon(rectImage);
        rectToggle = new JToggleButton("Rectangle", rectIcon);
        rectToggle.setName("Rectangle");
        modeButtonGroup.add(rectToggle);
        
        
        ImageIcon ovalIcon = new ImageIcon("resources/images/oval.jpeg", "");
        Image ovalImage = ovalIcon.getImage();
        ovalImage = ovalImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        ovalIcon = new ImageIcon(ovalImage);
        ovalToggle = new JToggleButton("Oval");
        ovalToggle.setName("Oval");
        modeButtonGroup.add(ovalToggle);
        
        strokeWidthLabel = new JLabel("Stroke Width");
        strokeWidthLabel.setName("Stroke Width Label");
        strokeWidthSlider = new JSlider(1, 20, 1);
        strokeWidthSlider.setName("Stroke Width");
        
        ImageIcon colorIcon = new ImageIcon("resources/images/color.png", "");
        Image colorImage = colorIcon.getImage();
        colorImage = colorImage.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
        colorIcon = new ImageIcon(colorImage);
        chooseColorButton = new JButton("Color", colorIcon);
        chooseColorButton.setName("Color");
        
        whiteboardNameLabel = new JLabel("Current Canvas: DEFAULT");
        whiteboardNameLabel.setName("whiteboardNameLabel");
        changeWhiteboardLabel = new JLabel("Add/Set Canvas Name: ");
        changeWhiteboardLabel.setName("Change Whiteboard Label");
        changeWhiteboard = new JTextField();
        changeWhiteboard.setName("Change Whiteboard");
        
        
        // initialize panel layout
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        setSize(new Dimension(width, height));

        // automatic gaps
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        
        // layout horizontal groups
        layout.setHorizontalGroup(
                layout.createParallelGroup()
                    .addComponent(drawToggle)
                    .addComponent(lineToggle)
                    .addComponent(rectToggle, 120, 120, 120)
                    .addComponent(ovalToggle)
                    .addComponent(eraseToggle)
                    .addComponent(fillToggle)
                    .addComponent(chooseColorButton)
                    .addComponent(strokeWidthLabel)
                    .addComponent(strokeWidthSlider)
                    .addComponent(whiteboardNameLabel)
                    .addComponent(changeWhiteboardLabel)
                    .addComponent(changeWhiteboard)
                );
        
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                    .addComponent(drawToggle)
                    .addComponent(lineToggle)
                    .addComponent(rectToggle)
                    .addComponent(ovalToggle)
                    .addComponent(eraseToggle)
                    .addComponent(fillToggle)
                    .addComponent(chooseColorButton)
                    .addComponent(strokeWidthLabel)
                    .addComponent(strokeWidthSlider)
                    .addComponent(whiteboardNameLabel)
                    .addComponent(changeWhiteboardLabel)
                    .addComponent(changeWhiteboard, 20, 20, 20)
                );
        
        //sets horizontal size of all buttons to be identical
        layout.linkSize(SwingConstants.HORIZONTAL, 
                drawToggle,
                eraseToggle,
                lineToggle, 
                rectToggle, 
                ovalToggle, 
                fillToggle, 
                strokeWidthLabel, 
                strokeWidthSlider, 
                chooseColorButton, 
                changeWhiteboardLabel, 
                changeWhiteboard);
       
        //add modeListener to each button (clicking a button changes mode)
        ModeListener modeListener = new ModeListener();
        drawToggle.addActionListener(modeListener);
        eraseToggle.addActionListener(modeListener);
        fillToggle.addActionListener(modeListener);
        lineToggle.addActionListener(modeListener);
        ovalToggle.addActionListener(modeListener);
        rectToggle.addActionListener(modeListener);
        
        //add ChangeListener to stroke slider, which changes the stroke width when
        //user drags the slider
        strokeWidthSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                strokeWidth = strokeWidthSlider.getValue();
            }           
        }); 
        
        //clicking the color button pulls up a JColorChooser dialog, which prompts
        //the user to choose a color
        chooseColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                color = JColorChooser.showDialog(chooseColorButton, "Choose Color", color);
                chooseColorButton.setForeground(color);
            }
        });
        
        //when the user inputs a whiteboard number to the textbox, change to the whiteboard
        //to the specified whiteboard
        changeWhiteboard.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField canvasID = (JTextField) e.getSource();
                String changeName = canvasID.getText();
                changeWhiteboard(changeName);
            }
        });
    }
    
    /** Returns an ImageIcon, or null if the path was invalid. 
     * code from: http://docs.oracle.com/javase/tutorial/uiswing/components/icon.html
     * */
    protected ImageIcon createImageIcon(String path,
                                               String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    
    /**
     * @param newWorkingID is ID of canvas that the user is switching to
     * 		-must be an integer between 0 and maxBoards
     */
    private void changeWhiteboard(String changeName){
        String name = changeName;
        if (changeName == null || changeName.matches("[ ]*")){
            name = "DEFAULT";
        }
        String label = "Current Canvas: " + name;
        Canvas canvas = (Canvas) this.getParent().getComponent(1);
        canvas.changeBoard(name);
        whiteboardNameLabel.setText(label);
    }
    
    /**
     * @return current stroke width of the palette
     */
    public synchronized int getStrokeWidth() {
        return strokeWidth;
    }
    
    /**
     * @return current color that the palette is set to
     */
    protected synchronized Color getColor() {
        return color;
    }
    
    /**
     * @return the RGB representation of the current color
     */
    public synchronized int getColorRGB() {
    	if(color != null)
    		return color.getRGB();
    	else
    		return 0; 
    }
    
    /**
     * Sets current Mode of the palette
     * @param newMode is a non-null Mode object 
     */
    protected synchronized void setMode(Mode newMode){
        mode = newMode;
    }
    
    /**
     * @return current Mode of the palette
     */
    public synchronized Mode getMode() {
        return mode;
    }
    
    /**
     * ModeListener checks for current mode of canvas (draw, erase,
     * fill, line, rect, or oval) by checking the name of the button pressed.
     * @author aanojima
     *
     */
    private class ModeListener implements ActionListener {                       
        @Override
        public void actionPerformed(ActionEvent e){
            JToggleButton mode = (JToggleButton) e.getSource();
            
            if (mode.getName().equals("Draw")) {
                setMode(Mode.DRAW);
            } else if (mode.getName().equals("Erase")) {
                setMode(Mode.ERASE);
            } else if (mode.getName().equals("Fill")) {
                setMode(Mode.FILL);
            } else if (mode.getName().equals("Line")) {
                setMode(Mode.LINE);
            } else if (mode.getName().equals("Rectangle")) {
                setMode(Mode.RECT);
            } else if (mode.getName().equals("Oval")) {
                setMode(Mode.OVAL);
            }
        }
    }
    
    /**
     * Displays palette on the window.
     * @param args
     */
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                Palette palette = new Palette(200, 600);
                JFrame window = new JFrame();
                window.setSize(200, 600);
                window.setContentPane(palette);
                window.setVisible(true);
            }
        });
    }
}