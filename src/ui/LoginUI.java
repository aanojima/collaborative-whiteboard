package ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/** TESTING STRATEGY
 * 
 *  	-LoginUI is displayed when client connects to server
 *  	-Contains textbox for user to input username
 */

/** THREAD-SAFETY ARGUMENT
 * 
 * 		-all fields are final (immutable)
 * 		-listeners are blocked on alias (the LoginUI), so multiple users cannot login simultaneously
 */

/**
 *  SPECIFICATIONS
 *  
 *  UserUI is the UI that shows up when clients connects to the server.
 *  It prompts the user for a username before displaying the canvas/palette window
 */

public class LoginUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private final LoginUI alias = this;
    
    private String username = null;
    
    private final JPanel pane;
    private final JLabel loginLabel;
    private final JButton login;
    private final JTextField input;
    
    public LoginUI(){
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Login");
        
        loginLabel = new JLabel("Create a username: ");
        loginLabel.setName("loginLabel");
        
        login = new JButton("Login");
        login.setName("login");
        
        input = new JTextField("");
        input.setName("input");
        
        pane = new JPanel();
        this.setContentPane(pane);
        this.setMinimumSize(new Dimension(400, 80));
        this.setResizable(false);
        
        // Initialize Layout
        GroupLayout layout = new GroupLayout(pane);
        pane.setLayout(layout);
        
        // Order Layout by Groups
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(
            layout.createSequentialGroup()
                .addComponent(loginLabel)
                .addComponent(input)
                .addComponent(login)
        );

        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addComponent(loginLabel)
                .addComponent(input, 20, 20, 20)
                .addComponent(login)
        );
        
        //add action listeners
        input.addActionListener(new LoginListener());
        login.addActionListener(new LoginListener());
        this.addWindowListener(new CloseListener());
        
        this.setVisible(true);
        
    }
    
    /**
     * @return username that client has inputed
     */
    public String getUsername(){
        return this.username;
    }
    
    /**
     * If client closes LoginUI without inputting an username, set the user's username
     * as "Anonymous User"
     */
    private class CloseListener implements WindowListener {

        @Override
        public void windowClosed(WindowEvent arg0) {
            // Locks Login UI and notifies once complete
            synchronized (alias){
                //empty input --> username = BLANK
                if (input.getText() == null || input.getText().matches("[ ]*")){
                    username = "BLANK";
                } else {
                    username = input.getText();
                }
                alias.notify();
            }
        }

        public void windowActivated(WindowEvent arg0){}
        public void windowClosing(WindowEvent arg0) {}
        public void windowDeactivated(WindowEvent arg0) {}
        public void windowDeiconified(WindowEvent arg0) {}
        public void windowIconified(WindowEvent arg0) {}
        public void windowOpened(WindowEvent arg0) {}
        
    }
    
    //sets username to user input when user logs in
    private class LoginListener implements ActionListener {
        
        public void actionPerformed(ActionEvent e){
         // Locks Login UI and notifies once complete
            synchronized (alias){
            	//empty input --> username = BLANK
                if (input.getText() == null || input.getText().matches("[ ]*")){
                    username = "BLANK";
                } else {
                    username = input.getText();
                }
                alias.notify();
            }
        }
    }
    
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new LoginUI();
            }
        });
    }
}
