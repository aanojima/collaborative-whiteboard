package ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.xml.bind.DatatypeConverter;

import controller.DrawingController;

/**
 * TESTING STRATEGY:
 * 
 * Check (in combination with Palette) that editing functions are working
 * 		- editing functions include: draw, erase, fill, drawRect, drawOval
 * See CanvasTest and PaletteTest in testing package for detailed test cases
 */

/**
 * THREAD-SAFETY ARGUMENT:
 * 
 * DRAWINGBUFFER is only accessible through synchronized canvas methods.  
 * 
 * PALETTE is a thread-safe data-type (see Palette Thread-Safety Argument).
 *   
 * NAME is only accessible through methods that lock the canvas.
 *   
 * USERNAME is immutable and only accessible through synchronized canvas.
 *   
 * ISMASTER is a constant primitive boolean for every instance of Canvas.
 *   
 * OUT is a PrintWriter with auto-flushing (see WhiteboardClient and WhiteboardServer).
 */

/**
 * SPECIFICATIONS
 * 
 * Canvas represents a drawing surface that allows the user to draw
 * on it freehand, with the mouse.
 * 
 * Each client has its own associated canvas that are edited only through
 * instructions sent from the server via drawing controller
 */

public class Canvas extends JPanel {

    private static final long serialVersionUID = 1L;
    
    // Image of User's Drawing
    private BufferedImage drawingBuffer;
    
    // Contains Personal User Graphics
    private Palette palette;
    
    // Name of Current Working Board
    private String name;
    
    // Name of User
    private final String username;
    
    // Server vs. Client
    private final boolean isMaster;
    
    // Output to Server
    private final PrintWriter out;
    
    /**Constructor 1 of Canvas. Associates server canvas to a drawing controller
     * @param width specifies the width of the canvas in pixels
     * @param height specifies the height of the canvas in pixels
     * @param palette is a Palette object which contains drawing tools
     * @param name is the starting board name
     */
    public Canvas(int width, int height, Palette palette, String name) {
        this.setSize(width, height);
        this.palette = palette;
        this.name = name;
        this.username = "MASTER";
        this.isMaster = true;
        this.out = null;
        addDrawingController(null);
    }
    
    /**
     * Constructor 2 of Canvas. Associates canvas to a drawing controller
     * @param width specifies the width of the canvas in pixels
     * @param height specifies the height of the canvas in pixels
     * @param palette is a Palette object which contains drawing tools
     * @param username is specified by client at login
     * @param in
     * @param out
     */
    public Canvas(int width, int height, Palette palette, String username, PrintWriter out) {
        this.setSize(width, height);
        this.palette = palette;
        this.name = "DEFAULT";
        this.username = username;
        this.isMaster = false;
        this.out = out;
        addDrawingController(out);
    }
        
    /**
     * @return name of the current canvas
     */
    public synchronized String getBoardName(){
        return this.name;
    }
    
    /**
     * @return username of associated client
     */
    protected String getUsername(){
        return username;
    }
    
    protected PrintWriter getOutput(){
        return out;
    }
    
    /**
     * Changes the current canvas
     * @param changeName is the name of the canvas the user is switching to
     */
    protected synchronized void changeBoard(String changeName){
        if (!isMaster){
            if (changeName.matches("[ ]*")){
                changeName = "DEFAULT";
            }
            this.name = changeName;
            out.println("CHANGE," + username + "," + changeName);
        }
    }
    
    /**
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    public synchronized void paintComponent(Graphics g) {
        // If this is the first time paintComponent() is being called,
        // make our drawing buffer.
        if (drawingBuffer == null) {
            makeDrawingBuffer();
            out.println("START," + username + ",DEFAULT");
        }

        Graphics2D g2 = (Graphics2D) g;
        
        g2.setColor(Color.BLACK);
        g2.setStroke(new BasicStroke());
        
        // Copy the drawing buffer to the screen.
        g.drawImage(drawingBuffer, 0, 0, null);
        
        this.repaint();

    }
    
    /**
     * Make the drawing buffer and fill it with a white background.
     */
    private synchronized void makeDrawingBuffer() {

        drawingBuffer = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        
        Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
        
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, getWidth(), getHeight());

        this.repaint();
    }    
    
    /**
     * @return String representing the drawingBuffer's image
     * @throws IOException
     */
    public String getDrawingBufferString() throws IOException {
        if (this.drawingBuffer == null){
            makeDrawingBuffer();
        }
        synchronized(drawingBuffer){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(this.drawingBuffer, "png", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            return DatatypeConverter.printBase64Binary(imageInByte);
        }
    }
    
    /**
     * @param x, the x-coordinate of the pixel
     * @param y, the y-coordinate of the pixel
     * @return the RGB integer representing the color the pixel (x, y)
     */    
    public int getImageRGB(int x, int y) {
        if (this.drawingBuffer == null) {
            makeDrawingBuffer();
        }
        return this.drawingBuffer.getRGB(x, y);
    }
    
    /**
     * Given data representing a buffered image, repaints the Canvas's drawingBuffer
     * as specified by the data string
     * @param data, the String representing an BufferedImage
     */
    public void setDrawingBuffer(String data){        
        synchronized(drawingBuffer){
            byte[] binary = DatatypeConverter.parseBase64Binary(data);
            ByteArrayInputStream bais = new ByteArrayInputStream(binary);
            try {
                this.drawingBuffer = ImageIO.read(bais);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.repaint();
        }
    }

    /**
     * Draws a line with endpoints specified by (x1, y1) and (x2, y2)
     * @param x1 is the x-coord of a pixel that lies on the canvas
     * @param y1 is the y-coord of a pixel that lies on the canvas
     * @param x2 is the x-coord of a pixel that lies on the canvas
     * @param y2 is the y-coord of a pixel that lies on the canvas
     * @param rgb is the color in rgb format (between 0 and 256)
     * @param strokeWidth specifies strokeWidth of pen (must be non-negative)
     */
    public void draw(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
        if (drawingBuffer == null){
            this.makeDrawingBuffer();
        }
        synchronized(drawingBuffer){
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
                
            g2.setColor(new Color(rgb));
            g2.setStroke(new BasicStroke(strokeWidth));
            g2.drawLine(x1, y1, x2, y2);
        }
    }

    /**
     * Erases the area specified by the coordinates (x1, y1) and (x2, y2)
     * @param x1 is the x-coord of a pixel that lies on the canvas
     * @param y1 is the y-coord of a pixel that lies on the canvas
     * @param x2 is the x-coord of a pixel that lies on the canvas
     * @param y2 is the y-coord of a pixel that lies on the canvas
     * @param strokeWidth specifies the strokeWidth (must be non-negative)
     */
    public void erase(int x1, int y1, int x2, int y2, int strokeWidth) {        
        if (this.drawingBuffer == null){
            this.makeDrawingBuffer();
        }
        synchronized(drawingBuffer){
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();

            g2.setColor(Color.WHITE);
            g2.setStroke(new BasicStroke(strokeWidth));
            g2.drawLine(x1, y1, x2, y2);
            this.repaint();
        }
    }
    
   /** (x,y) is starting place of the fill. Neighboring pixels are
     * recursively filled to the specified palette color if they are 
     * the same color as (x,y).
     * @param x is the x-coord of a pixel that lies on the canvas
     * @param y is the y-coord of a pixel that lies on the canvas
     * @param rgb is the color in rgb format (between 0 and 256)
     */
    public void fill(int x, int y, int rgb) {
        if (this.drawingBuffer == null){
            this.makeDrawingBuffer();
        }
        synchronized(drawingBuffer){
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            
            g2.setColor(new Color(rgb));
            int fillRGB = g2.getColor().getRGB();
            int regionRGB = drawingBuffer.getRGB(x, y);
            
            if (regionRGB == rgb) return;
            
            Queue<int[]> pixelQueue = new LinkedList<int[]>();
            pixelQueue.add(new int[] {x, y});        
            int[] pixel;
            int nx, ny;
            
            while (!pixelQueue.isEmpty()) {
                pixel = pixelQueue.poll();
                nx = pixel[0];
                ny = pixel[1];
                if (drawingBuffer.getRGB(nx, ny) == regionRGB) {
                    drawingBuffer.setRGB(nx, ny, fillRGB);
                    
                    for (int[] neighbor : getNeighborPixels(nx, ny)) {
                        if (drawingBuffer.getRGB(neighbor[0], neighbor[1]) != fillRGB) {
                            pixelQueue.add(new int[] {neighbor[0], neighbor[1]});
                        }
                    }
                }
                this.repaint();
            }
        }
    }
    
    /** Draws a rectangle with opposite vertices given by (x1, y1) and (x2, y2)
     * @param x1 is the x-coord of a pixel that lies on the canvas
     * @param y1 is the y-coord of a pixel that lies on the canvas
     * @param x2 is the x-coord of a pixel that lies on the canvas
     * @param y2 is the y-coord of a pixel that lies on the canvas
     * @param strokeWidth specifies the strokeWidth (must be non-negative)
     * @param rgb is a rgb value for the Color object that specifies color of rectangle
     */
    public void drawRect(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
        if (this.drawingBuffer == null){
            this.makeDrawingBuffer();
        }
        synchronized(drawingBuffer){
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            g2.setColor(new Color(rgb));
            g2.setStroke(new BasicStroke(strokeWidth));
            
            g2.drawRect(
                    (x1 < x2) ? x1 : x2,
                    (y1 < y2) ? y1 : y2,
                    Math.abs(x1 - x2),
                    Math.abs(y1 - y2)
                    );
            
            this.repaint();
        }
    }
    
    /** Draws an oval with a diameter specified by the line between
     * (x1, y1) and (x2, y2)
     * @param x1 is the x-coord of a pixel that lies on the canvas
     * @param y1 is the y-coord of a pixel that lies on the canvas
     * @param x2 is the x-coord of a pixel that lies on the canvas
     * @param y2 is the y-coord of a pixel that lies on the canvas
     * @param strokeWidth specifies the strokeWidth (must be non-negative)
     * @param color is a Color object that specifies color of oval
     */
    public void drawOval(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
        if (this.drawingBuffer == null){
            this.makeDrawingBuffer();
        }
        Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
        
        g2.setColor(new Color(rgb));
        g2.setStroke(new BasicStroke(strokeWidth));
        
        g2.drawOval(
                (x1 < x2) ? x1 : x2,
                (y1 < y2) ? y1 : y2,
                Math.abs(x1 - x2),
                Math.abs(y1 - y2));
        this.repaint();
    }
    
    /**
     * @param x is the x coordinate of a pixel within the canvas
     * @param y is the y coordinate of a pixel within the canvas
     * @return a 2D integer array containing the coordinates of
     * the neighboring pixels of (x, y)
     */
    private int[][] getNeighborPixels(int x, int y) {
        return new int[][] {
            {Math.min(x + 1, getWidth() - 1), y},  //right neighbor
            {Math.max(x - 1, 0), y},               //left neighbor
            {x, Math.min(y + 1, getHeight() - 1)}, //up neighbor
            {x, Math.max(y - 1, 0)}                //down neighbor
        };
    }
       
    /**
     * Add the mouse listener that supports the user's freehand drawing.
     */
    private void addDrawingController(PrintWriter out) {
        DrawingController controller = new DrawingController(this, palette, out, username);
        addMouseListener(controller);
        addMouseMotionListener(controller);
    }
}
