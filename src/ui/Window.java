package ui;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * TESTING STRATEGY
 * 
 * 		-Window is displayed after user logs in
 *		-Window contains canvas, palette, and table of users
 */

/**
 * THREAD-SAFETY ARGUMENT
 * 
 *		1. Canvas is a thread-safe datatype
 *		2. Palette is a thread-safe datatype
 *		3. UsersUI is a thread-safe datatype
 */

/**
 * SPECIFICATIONS
 * 
 * Window displays Canvas, Palette, and UsersUI (table of connected users)
 * Windows is displayed after user logs in
 */

public class Window extends JFrame {
    
    private static final long serialVersionUID = 1L;
    private final Canvas canvas;
    private final Palette palette;
    private final UsersUI clients;
    
    /**
     * Display Window
     * @param canvas is Canvas object to be displayed
     * @param palette is Palette object to be displayed
     * @param clients is table that displays all connected users
     */
    public Window(Canvas canvas, Palette palette, UsersUI clients){
        
        // Window Components
        this.canvas = canvas;
        this.palette = palette;
        this.clients = clients;
                
        int cWidth = this.canvas.getWidth();
        int cHeight = this.canvas.getHeight();
        int pWidth = this.palette.getWidth();
        int pHeight = this.palette.getHeight();
        int uWidth = this.clients.getWidth();
        int uHeight = this.clients.getHeight();
        
        this.setTitle(canvas.getUsername()+ "'s Whiteboard");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
        this.addWindowListener(new CloseListener());
        final JPanel content = new JPanel();
        
        // Create Layout
        GroupLayout layout = new GroupLayout(content);
        content.setLayout(layout);
        
        // Horizontal Layout
        layout.setHorizontalGroup(
            layout.createSequentialGroup()
                .addComponent(this.palette, pWidth, pWidth, pWidth)
                .addComponent(this.canvas, cWidth, cWidth, cWidth)
                .addComponent(this.clients, uWidth, uWidth, uWidth)
        );
        
        // Vertical Layout
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addComponent(this.palette, pHeight, pHeight, pHeight)
                .addComponent(this.canvas, cHeight, cHeight, cHeight)
                .addComponent(this.clients, uHeight, uHeight, uHeight)
        );
        
        // Setup and Initialize Window
        int width = pWidth + cWidth + uWidth + 3*10;
        int height = cHeight + 50;
        this.setContentPane(content);
        this.setSize(width, height);
        this.setMaximumSize(new Dimension(width, height));
        this.setMinimumSize(new Dimension(width, height));
        this.setVisible(true);
    }
    
    private class CloseListener implements WindowListener {
        @Override
        public void windowClosed(WindowEvent arg0) {
            canvas.getOutput().println("EXIT" + "," + canvas.getUsername());
        }

        public void windowActivated(WindowEvent arg0){}
        public void windowClosing(WindowEvent arg0) {}
        public void windowDeactivated(WindowEvent arg0) {}
        public void windowDeiconified(WindowEvent arg0) {}
        public void windowIconified(WindowEvent arg0) {}
        public void windowOpened(WindowEvent arg0) {}
        
    }
    
}
