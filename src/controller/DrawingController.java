package controller;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.PrintWriter;

import ui.Canvas;
import ui.Palette;

/**
 * TESTING STRATEGY
 * 
 * 	1. Mouse clicked action works (fill)
 * 
 * 	2. Mouse released action works (LINE, RECT, OVAL)
 * 
 * 	3. Mouse dragged action works (DRAW, ERASE, RECT, OVAL)
 * 
 * 	4. Mouse pressed action works (returns coordinate of current point mouse in on)
 *
 */

/**
 * THREAD-SAFETY ARGUMENT
 * 
 * 	1. Canvas is a thread-safe datatype
 * 
 * 	2. Palette is a thread-safe datatype
 * 
 * 	3. PrintWriter is a PrintWriter with auto-flushing which handles synchronization internally
 * 
 * 	4. username is a String, which is immutable.
 * 
 * 	5. lastPoint is a private variable (cannot be accessed outside the class).
 */

/**
 * SPECIFICATIONS
 *
 * DrawingController represents the communication channel between client and server. It translates each
 * action on the canvas into an instruction string, which is interpreted by server/client to perform the 
 * specified action. 
 */

public class DrawingController implements MouseListener, MouseMotionListener {
    
    // store the coordinates of the last mouse event, so we can
    // draw a line segment from that last point to the point of the next mouse event.
    private final Canvas canvas;
    private final Palette palette;
    private final PrintWriter out;
    private final String username;
    private Point lastPoint;
    
    /**
     * Sets the DrawingController's canvas, palette, PrintWriter, and username
     * @param canvas is Canvas object associated with this DrawingController
     * @param palette is Palette object associated with this DrawingController
     * @param out is a PrintWriter with auto-flushing which handles synchronization internally
     * @param username is used to identify the user associated with this DrawingController
     */
    public DrawingController(Canvas canvas, Palette palette, PrintWriter out, String username) {
        this.canvas = canvas;
        this.palette = palette;
        this.out = out;
        this.username = username;
    }

    /**
     * When mouse button is pressed down, start drawing.
     */
    public void mousePressed(MouseEvent e) {
        lastPoint = e.getPoint();
    }

    /**
     * When mouse is dragged, output instructions to draw the appropriate object given the mode 
     * (DRAW, ERASE, LINE, RECT, OVAL) canvas is currently set to.
     * Instructions are in the format: MODE, username, canvasID, lastPointX, lastPointY, currentPointX, 
     * currentPointY, color(in RGB), strokeWidth. 
     */
    public void mouseDragged(MouseEvent e) {
        Point point = e.getPoint();
        final String instructions;        
        switch(palette.getMode()){
            
            case DRAW:
                instructions =
                            "DRAW," + username + "," + this.canvas.getBoardName() + "," +
                            String.valueOf(lastPoint.x) + "," +
                            String.valueOf(lastPoint.y) + "," +
                            String.valueOf(point.x) + "," +
                            String.valueOf(point.y) + "," +
                            String.valueOf(palette.getColorRGB()) + "," +
                            String.valueOf(palette.getStrokeWidth());
                out.println(instructions);
                lastPoint = point;
                break;
                
            case ERASE:
                instructions =
                            "ERASE," + username + "," + this.canvas.getBoardName() + "," + 
                            String.valueOf(lastPoint.x) + "," + 
                            String.valueOf(lastPoint.y) + "," +
                            String.valueOf(point.x) + "," +
                            String.valueOf(point.y) + "," +
                            String.valueOf(palette.getStrokeWidth());
                out.println(instructions);
                lastPoint = point;
                break;
            case LINE:
                instructions = "LINE," + username + "," + this.canvas.getBoardName() + "," +
                				String.valueOf(lastPoint.x) + "," +
                				String.valueOf(lastPoint.y) + "," +
                                String.valueOf(point.x) + "," +
                                String.valueOf(point.y) + "," +
                                String.valueOf(this.palette.getStrokeWidth()) + "," +
                                String.valueOf(this.palette.getColorRGB());
                break;
                
            case RECT:
            	instructions = "RECT," + username + "," + this.canvas.getBoardName() + "," +
        				String.valueOf(lastPoint.x) + "," +
        				String.valueOf(lastPoint.y) + "," +
                        String.valueOf(point.x) + "," +
                        String.valueOf(point.y) + "," +
                        String.valueOf(this.palette.getStrokeWidth()) + "," +
                        String.valueOf(this.palette.getColorRGB());
                break;
                
            case OVAL:
            	instructions = "OVAL," + username + "," + this.canvas.getBoardName() + "," +
        				String.valueOf(lastPoint.x) + "," +
        				String.valueOf(lastPoint.y) + "," +
                        String.valueOf(point.x) + "," +
                        String.valueOf(point.y) + "," +
                        String.valueOf(this.palette.getStrokeWidth()) + "," +
                        String.valueOf(this.palette.getColorRGB());
                break;
            
            default:
                break;
        }
    }
    
    /**
     * The only action performed with a mouse click is fill.
     * Instructions are given in format: FILL, username, canvasID, currentPointX, currentPointY, color
     */
    public void mouseClicked(MouseEvent e) {
        Point point = e.getPoint();
        final String instructions;
        switch(palette.getMode()) {
            case FILL:
                instructions =
                            "FILL," + username + "," + this.canvas.getBoardName() + "," + 
                            String.valueOf(point.x) + "," + 
                            String.valueOf(point.y) + "," +
                            String.valueOf(palette.getColorRGB());
                out.println(instructions);
                break;
            default:
                break;
        }
    }

    public void mouseMoved(MouseEvent e) { }
    
    /**
     * When the mouse is released, output instructions given the current mode (LINE, RECT, or OVAL).
     * Instructions are in format: MODE, username, canvasID, lastPointX, lastPointY, currentPointX, 
     * currentPointY, color, strokeWidth
     */
    public void mouseReleased(MouseEvent e) {
        Point point = e.getPoint();
        final String instructions;
        
        switch(palette.getMode()) {
            case LINE:
                instructions =
                            "DRAW," + username + "," + this.canvas.getBoardName() + "," + 
                            String.valueOf(lastPoint.x) + "," + 
                            String.valueOf(lastPoint.y) + "," +
                            String.valueOf(point.x) + "," +
                            String.valueOf(point.y) + "," +
                            String.valueOf(palette.getColorRGB()) + "," +
                            String.valueOf(palette.getStrokeWidth());
                out.println(instructions);
                break;
                
            case RECT:
                //TCP Method
            	instructions = "RECT," + username + "," + this.canvas.getBoardName() + "," +
            			String.valueOf(lastPoint.x) + "," +
        				String.valueOf(lastPoint.y) + "," +
                        String.valueOf(point.x) + "," +
                        String.valueOf(point.y) + "," +
                        String.valueOf(palette.getColorRGB()) + "," +
                        String.valueOf(palette.getStrokeWidth());
            	out.println(instructions);
                break;
                
            case OVAL:
                //TCP Method
            	instructions = "OVAL," + username + "," + this.canvas.getBoardName() + "," +
        				String.valueOf(lastPoint.x) + "," +
        				String.valueOf(lastPoint.y) + "," +
                        String.valueOf(point.x) + "," +
                        String.valueOf(point.y) + "," +
                        String.valueOf(palette.getColorRGB()) + "," +
                        String.valueOf(palette.getStrokeWidth());
            	out.println(instructions);
                break;
            default:
                break;
        }
        
    }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
}