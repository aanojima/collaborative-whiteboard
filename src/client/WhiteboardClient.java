package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import ui.Canvas;
import ui.LoginUI;
import ui.Palette;
import ui.UsersUI;
import ui.Window;

/**
 * TESTING STRATEGY
 * 
 * 1. Instantiates the client when client logs in. A client who enters
 *    a blank, space-only, or username that is already taken 
 *    will be asked to re-enter a new username). 
 * 
 * 2. Connect to the server without error, handles, and correctly interprets the messages from the server
 * 
 * 3. Supports host and port arguments when running.
 *   
 */

/**
 * THREAD-SAFETY ARGUMENT
 * 
 * USERNAME is an immutable object (string) and is only reassigned while WhiteboardClient
 *      is being constructed. 
 *      
 * USERS is only accessible through WhiteboardClient methods. 
 *      It is always locked whenever it is called. 
 * 
 * CANVAS is a thread-safe data-type (see Canvas Thread-Safety Argument)
 * 
 * PALETTE is a thread-safe data-type (see Palette Thread-Safety Argument)
 * 
 * USERSUI is a thread-safe data-type (see UsersUI Thread Safety Argument)
 * 
 * SOCKET is only accessible in WhiteboardClient methods to get the IO Streams and to be closed.   
 * 
 * IN is a BufferedReader which takes in lines one by one like a queue and handles synchronization internally.  
 * 
 * OUT is a PrintWriter with auto-flushing which handles synchronization internally. 
 * 
 * DIMENSIONS are all constant integers. 
 *  
 */ 

/**
 * SPECIFICATIONS
 * 
 * WhiteboardClient is the main file the user will run. Takes arguments
 * HOST and PORT and connects to the server with the same conditions. 
 * Each client runs its own thread. When a client makes an edit
 * instructions are sent to the server (via drawing controller) and the client
 * updates the canvas based on the instructions received from the server. 
 * 
 * USERNAME is the publicly displayed name of the client
 * 
 * USERS is an array list of all other clients (as username) connected currently
 * 
 * CANVAS is the client's personal canvas that displays the client's and others' drawing actions
 * 
 * PALETTE is the client's personal palette of graphics settings
 * 
 * USERSUI is the available users display that shows everyone but the client's own username
 * 
 * SOCKET is the socket the client uses to connect to the server
 * 
 * IN is the buffered reader from the input stream of the client socket
 * 
 * OUT is the print writer from the output stream of the client socket
 * 
 * DIMENSIONS are constant integer values for the size of specified GUIs
 * 
 */

public class WhiteboardClient {
    
    // USER DATA
    private String username;
    private final ArrayList<String> users;
    
    // VIEWS
    private final Canvas canvas;
    private final Palette palette;
    private final UsersUI usersUI;
    
    // COMMUNICATIONS
    private final Socket socket;
    private final BufferedReader in;
    private final PrintWriter out;
    
    // DIMENSIONS
    private final static int paletteWidth = 225;
    private final static int paletteHeight = 600;
    private final static int canvasWidth = 600;
    private final static int canvasHeight = 600;
    private final static int usersWidth = 100;
    private final static int usersHeight = 600;
    
    /**
     * Instantiates client that connects to server using address.
     * @param address is the InetAddress hostname or IP-address of the socket to be used
     * @param port is the port number to which the client is connecting
     * from 1 to 65534.  
     * @throws IOException invalid socket IO
     */
    public WhiteboardClient(InetAddress address, int port) throws IOException{
        
        this.palette = new Palette(paletteWidth, paletteHeight);
        this.socket = new Socket(address, port);
        this.users = new ArrayList<String>();
        
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(socket.getOutputStream(), true);
        
        String testUsername = null;
        String response;
        // If server tells client to retry (invalid username), then repeatedly reopen login screen
        for (response = "RETRY"; response.matches("RETRY"); response = in.readLine()){                    
            LoginUI login =  new LoginUI();            
            // Wait for login information to be available
            synchronized(login){
                try{
                    login.wait();
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            testUsername = login.getUsername();            
            // Sends new user message to server
            String message = "USER" + "," + testUsername;
            synchronized(out){ out.println(message); }            
            // Close Login
            login.setVisible(false);
            login.dispose();
        }
        
        // Set username and initially add existing users to users
        username = testUsername;
        String[] usersArray = response.split(",");
        for (String user : usersArray){       
            synchronized(users){
                users.add(user);
            }
        }
        
        // Instantiates user interfaces (canvas, palette, users) in a new window
        this.usersUI = new UsersUI(usersWidth, usersHeight, usersArray);
        this.canvas = new Canvas(canvasWidth, canvasHeight, palette, username, out);
        new Window(this.canvas, this.palette, this.usersUI);
        
    }
    
    /**
     * Connects and runs the core function of the client
     */
    private void connect(){
        Thread update = new Thread(new Runnable() {
            public void run() {
                try {
                    handleResponse();
                } catch (IOException e) {}
            }
        });
        update.start();
    }
    
    /**
     * Handles the response from server (via drawing controller). 
     * @throws IOException for invalid input/output
     */
    private void handleResponse() throws IOException {
        for (String line = in.readLine(); line != null; line = in.readLine()){
            // METHOD, CLIENT, BOARD, OPTIONS
            String[] tokens = line.split(",");
            if (tokens[0].matches("USER")){
                // Add new user
                String client = tokens[1];
                if (client.matches(username)){
                    // Don't add self
                    continue;
                } else {
                    usersUI.addUser(client);
                    synchronized(users){                        
                        users.add(client);
                    }
                }
            } else if (tokens[0].matches("START") || tokens[0].matches("CHANGE")){
                // Set board and image
                String client = tokens[1];
                if (client.matches(username)){
                    canvas.setDrawingBuffer(tokens[3]);  // Token 3 should be the board image string
                }
            } else if (tokens[0].matches("EXIT")){
            	// User closes window (disconnects)
                String client = tokens[1];
                if (client.matches(username)){
                    // User who exited
                    break;
                } else {
                    // Remove specified user
                    synchronized(users){
                        users.remove(client);
                    }
                    usersUI.removeUser(client);
                }
            } else if (tokens[0].matches("DRAW|ERASE|FILL|RECT|OVAL")){
            	// User edits canvas
                String board = tokens[2];
                // Ignore if client not viewing that board
                if (!canvas.getBoardName().matches(board)){
                    continue;
                } else if (tokens[0].matches("DRAW")){
                    canvas.draw(
                            Integer.valueOf(tokens[3]), // First X
                            Integer.valueOf(tokens[4]), // First Y
                            Integer.valueOf(tokens[5]), // Last X
                            Integer.valueOf(tokens[6]), // Last Y
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                } else if (tokens[0].matches("ERASE")){
                    canvas.erase(
                            Integer.valueOf(tokens[3]), // First X
                            Integer.valueOf(tokens[4]), // Firxt Y
                            Integer.valueOf(tokens[5]), // Last X
                            Integer.valueOf(tokens[6]), // Last Y
                            Integer.valueOf(tokens[7])); // Stroke Width
                
                } else if (tokens[0].matches("FILL")){
                    canvas.fill(
                            Integer.valueOf(tokens[3]), // X
                            Integer.valueOf(tokens[4]), // Y
                            Integer.valueOf(tokens[5])); // Color RGB
                } else if (tokens[0].matches("RECT")){
                    canvas.drawRect(
                            Integer.valueOf(tokens[3]), // First X
                            Integer.valueOf(tokens[4]), // First Y
                            Integer.valueOf(tokens[5]), // Last X
                            Integer.valueOf(tokens[6]), // Last Y
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                } else if (tokens[0].matches("OVAL")){
                    canvas.drawOval(
                            Integer.valueOf(tokens[3]), // First X
                            Integer.valueOf(tokens[4]), // First Y
                            Integer.valueOf(tokens[5]), // Last X
                            Integer.valueOf(tokens[6]), // Last Y
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                }
            } else {
                // Invalid Instructions
                continue;
            }
        }
        
        // Client exited
        socket.close();
        System.exit(0);
    }
    
    public static void main(String[] args) throws UnknownHostException {
        
        // Default Port and Number of Boards
        InetAddress address = InetAddress.getByName("localhost");
        int port = 4444;
        
        // Read Arguments
        Queue<String> arguments = new LinkedList<String>(Arrays.asList(args));
        try {
            while ( ! arguments.isEmpty()) {
                String flag = arguments.remove();
                try {
                    if (flag.equals("-host")) {
                        String host = arguments.remove();
                        address = InetAddress.getByName(host);
                    } else if (flag.equals("-port")) {
                        port = Integer.parseInt(arguments.remove());
                        if (port < 0 || port > 65535) {
                            throw new IllegalArgumentException("port " + port + " out of range");
                        }
                    } else {
                        throw new IllegalArgumentException("unknown option: \"" + flag + "\"");
                    }
                } catch (NoSuchElementException nsee) {
                    throw new IllegalArgumentException("missing argument for " + flag);
                } catch (NumberFormatException nfe) {
                    throw new IllegalArgumentException("unable to parse number for " + flag);
                }
            }
        } catch (IllegalArgumentException iae) {
            System.err.println(iae.getMessage());
            System.err.println("usage: WhiteboardClient [-host HOST] [-port PORT]");
        }
        
        // Start the Client
        try {
            WhiteboardClient client = new WhiteboardClient(address, port);
            client.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
