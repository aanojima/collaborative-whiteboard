package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import ui.Canvas;
import ui.Palette;

/**
 * TESTING STRATEGY
 * 
 * 1. Runs WhiteboardServer with arguments
 * 
 * 2. Instantiates the correct number of canvases/palettes in whiteboards
 * 
 * 3. Server accepts incoming connections without error
 * 
 * 4. Correctly interprets messages from the client's drawing controller, 
 *      updates master canvas and sends messages to all client(s) without error
 * 
 * 5. Client and the corresponding output is removed from the server (users) when client disconnects
 */

/**
 * THREAD-SAFETY ARGUMENT
 * 
 * SERVERSOCKET is accessed exclusively within WhiteboardServer method 'serve()' 
 *      and is never confined to a thread.    
 * 
 * WHITEBOARDS is only accessible through WhiteboardServer methods and is synchronized
 *      whenever called or updated. The canvases accessible from whiteboards are all also 
 *      thread-safe data-types (see Canvas Thread-Safety Argument)
 * 
 * USERS is only accessible through WhiteboardClient methods and is always 
 *      locked whenever it is called. 
 * 
 * OUTPUTS is only accessible through WhiteboardServer methods and is always
 *      locked whenever it is called.   
 * 
 * DIMENSIONS are all constant integers. 
 * 
 */

/**
 * SPECIFICATIONS
 * 
 * WhiteboardServer is the main file the server will run. Takes arguments
 * HOST and PORT and creates a server socket with the conditions. 
 * The server forever waits for a client connection and creates a socket from the connection.  
 * The server repeatedly waits until it receives instructions (via drawing controllers) and then
 * modifies its internal canvas based on the instruction. It then starts multiple
 * threads that send the instructors to each client (one per client).
 * 
 * SERVERSOCKET is the Socket the server will occupy to accept connections
 * 
 * WHITEBOARDS is the array list of Canvas canvases that will act as master data (with image)
 *      for all clients
 * 
 * USERS is an array list of String usernames all active clients connected currently
 * 
 * OUTPUTS is an array list of PrintWriter outputs (one from each client socket's output stream)
 * 
 * DIMENSIONS are constant integer values for the size of specified GUIs
 * 
 */

public class WhiteboardServer {
    
    // SERVER ENVIRONMENT
    private final ServerSocket serverSocket;
    
    // MASTER DATA
    private final Hashtable<String, Canvas> whiteboards;
    
    // CLIENT INFORMATION
    private final ArrayList<String> users;
    private final ArrayList<PrintWriter> outputs;

    // DIMENSIONS
    private final static int paletteWidth = 200;
    private final static int paletteHeight = 600;
    private final static int canvasWidth = 600;
    private final static int canvasHeight = 600;
    
    /**
     * Initialize WhiteboardServer 
     * Creates serverSocket for server. 
     * Creates maxBoards number of canvases/palettes. 
     * @param address is the InetAddress hostname or IP-address of the server's socket. 
     * @param port is the port number to which the server is using from 1 to 65534. 
     * @throws IOException for serverSocket IO
     */
    public WhiteboardServer(InetAddress address, int port) throws IOException {
        
        this.serverSocket = new ServerSocket(port, 0, address);
        
        this.whiteboards = new Hashtable<String, Canvas>();
        
        this.users = new ArrayList<String>();
        this.outputs = new ArrayList<PrintWriter>();

        // Master Canvases/Palettes     
        Palette palette = new Palette(paletteWidth, paletteHeight);
        Canvas canvas = new Canvas(canvasWidth, canvasHeight, palette, "DEFAULT");
        synchronized(whiteboards){
            this.whiteboards.put("DEFAULT", canvas);
        }
            
    }
    
    /**
     * Core Function of the Server
     * Blocks until a client connects. Passes on client's socket to a handler method. 
     * @throws IOException for invalid clientSocket accepted. 
     */
    private void serve() throws IOException {
        while (true) {
            
            // Wait for a User to Connect
            final Socket clientSocket = serverSocket.accept();

            // Handle User Connection
            final Thread clientThread = new Thread(new Runnable() {
                public void run() {   
                    try {
                        handleConnection(clientSocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }                 
                }            
            });          
            clientThread.start();
        }
    }
    
    /**
     * Handles A Client Connection. 
     * Creates in and out from IO stream of the client socket (saves out). 
     * Receives and Sends Instructions:
     *      USER - validate username, add user, and tell each client new list of usernames
     *      START/SWITCH - get master canvas image and send to user
     *      EXIT - remove user and tell each client who was removed
     *      DRAW|ERASE|FILL|RECT|OVAL - imitates user drawing operation on master canvas
     *          and sends same instructions to all client (see Canvas)
     * @param clientSocket is a Socket object representing the client end of the connection
     * @throws IOException invalid IO
     */
    private void handleConnection(final Socket clientSocket) throws IOException {
        
        final BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        final PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        synchronized(outputs){ outputs.add(out); }
        
        for (String line = in.readLine(); line != null; line = in.readLine()){
            String instructions = line;
            String[] tokens = line.split(",");
            // New user connection
            if (tokens[0].matches("USER")){
                String username = tokens[1];
                String usersString = null;
                synchronized(users){
                    if (username == null || users.contains(username) || username.matches("BLANK")){ // Username already used by another client
                        synchronized(out){ out.println("RETRY"); }
                        continue;
                    } else { // Send new list of all users to client
                        usersString = "";
                        users.add(username);
                        for (int i = 0; i < users.size(); i++){
                            String user = users.get(i);
                            usersString += user;
                            if (i < users.size() - 1){
                                usersString += ",";
                            }
                        }
                    }
                }
                synchronized(out){ out.println(usersString); }
                updateClients(instructions);
                
            } else if (tokens[0].matches("START") || tokens[0].matches("CHANGE")){
            	// User requires board image or has switched boards
                String board = tokens[2];
                Canvas serverCanvas;
                synchronized(whiteboards) {
                    if (whiteboards.containsKey(board)){
                        // Get existing canvas
                        serverCanvas = whiteboards.get(board);
                    } else {
                        // Add new canvas
                        Palette palette = new Palette(paletteWidth, paletteHeight);
                        serverCanvas = new Canvas(canvasWidth, canvasHeight, palette, board);
                        whiteboards.put(board, serverCanvas);
                    }
                }
                String data = serverCanvas.getDrawingBufferString();
                instructions = instructions + "," + data;
                updateClients(instructions);
            } else if (tokens[0].matches("EXIT")){
            	// User closes window (disconnects)
                String client = tokens[1];
                synchronized(users){
                    users.remove(client);
                }
                updateClients(instructions);
            } else if (tokens[0].matches("DRAW|ERASE|FILL|RECT|OVAL")){    
            	// User makes an edit to canvas
                String board = tokens[2];
                Canvas serverCanvas;
                synchronized(whiteboards){
                    serverCanvas = whiteboards.get(board);
                }
                if (tokens[0].matches("DRAW")){
                    serverCanvas.draw(
                            Integer.valueOf(tokens[3]), // x start point
                            Integer.valueOf(tokens[4]), // y start point
                            Integer.valueOf(tokens[5]), // x end point
                            Integer.valueOf(tokens[6]), // y end point
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                } else if (tokens[0].matches("ERASE")){
                    serverCanvas.erase(
                            Integer.valueOf(tokens[3]), // x start point
                            Integer.valueOf(tokens[4]), // y start point
                            Integer.valueOf(tokens[5]), // x end point
                            Integer.valueOf(tokens[6]), // y end point
                            Integer.valueOf(tokens[7])); // Stroke Width
                } else if (tokens[0].matches("FILL")){
                    serverCanvas.fill(
                            Integer.valueOf(tokens[3]), // x start point
                            Integer.valueOf(tokens[4]), // y start point
                            Integer.valueOf(tokens[5])); // Color RGB
                } else if (tokens[0].matches("RECT")){
                    serverCanvas.drawRect(
                            Integer.valueOf(tokens[3]), // x start point                                
                            Integer.valueOf(tokens[4]), // y start point
                            Integer.valueOf(tokens[5]), // x end point
                            Integer.valueOf(tokens[6]), // y end point
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                    
                } else if (tokens[0].matches("OVAL")){
                    serverCanvas.drawRect(
                            Integer.valueOf(tokens[3]), // x start point
                            Integer.valueOf(tokens[4]), // y start point
                            Integer.valueOf(tokens[5]), // x end point
                            Integer.valueOf(tokens[6]), // y end point
                            Integer.valueOf(tokens[7]), // Color RGB
                            Integer.valueOf(tokens[8])); // Stroke Width
                }
                updateClients(instructions);
            }
        }
    }
    
    /**
     * Update ALL Clients
     * Creates a thread for each client and sends instructions
     * to each client (one per client)
     * @param instructions tells client what to do
     */
    private void updateClients(final String instructions){
        synchronized(outputs){
            for (int i = 0; i < outputs.size(); i++){
                final PrintWriter out = outputs.get(i);
                Thread thread = new Thread(new Runnable(){
                    public void run(){
                        synchronized(out){ out.println(instructions); }
                    }
                });
                thread.start();
            }
        }
    }
    
    public static void main(String[] args) throws UnknownHostException {
        
        // Default Port and Number of Boards
        int port = 4444;
        InetAddress address = InetAddress.getByName("localhost");
        
        // Read Arguments
        Queue<String> arguments = new LinkedList<String>(Arrays.asList(args));
        try {
            while ( ! arguments.isEmpty()) {
                String flag = arguments.remove();
                try {
                    if (flag.equals("-host")) {
                        address = InetAddress.getByName(arguments.remove());
                    } else if (flag.equals("-port")) {
                        port = Integer.parseInt(arguments.remove());
                        if (port < 0 || port > 65535) {
                            throw new IllegalArgumentException("port " + port + " out of range");
                        }
                    } else {
                        throw new IllegalArgumentException("unknown option: \"" + flag + "\"");
                    }
                } catch (NoSuchElementException nsee) {
                    throw new IllegalArgumentException("missing argument for " + flag);
                } catch (NumberFormatException nfe) {
                    throw new IllegalArgumentException("unable to parse number for " + flag);
                }
            }
        } catch (IllegalArgumentException iae) {
            System.err.println(iae.getMessage());
            System.err.println("usage: WhiteboardServer [-host HOST] [-port PORT]");
        }
        
        // Start the Server
        try {
            WhiteboardServer server = new WhiteboardServer(address, port);
            server.serve();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}