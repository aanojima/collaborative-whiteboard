package testing;

import org.junit.Test;

public class PaletteTest {
	
	@Test
	public void testDisplay() {
		//Palette is displayed in a window to the left of canvas.
		//Contains 7 buttons: Draw, Erase, Rect, Oval, Line, Fill, and Color
		//Contains slider: strokeWidthSlider
		//Contains JTextbox for user to input canvasID
		boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
	}
	
	@Test
	public void testListeners() {
		//Draw, Erase, Rect, Oval, Line, and Fill buttons are linked with ModeListener, 
		//which changes the mode of canvas when a button is clicked.
		//Draw clicked --> Mode = draw
		//Erase clicked --> Mode = erase
		//Rect clicked --> Mode = rect
		//Oval clicked --> Mode = oval
		//Line clicked --> Mode = line
		//Fill clicked --> Mode = fill
		
		//When Color button is clicked, current color is set to specified color
		//When strokedWidthSlider is dragged, current strokeWidth is set to specified width
		//When enter is clicked and a number between 0 and maxBoards is in the canvasID textbox,
		//canvas is changed to canvas specified by the number is the textbox
		
		boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
	}
}