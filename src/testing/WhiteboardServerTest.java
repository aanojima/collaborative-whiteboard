package testing;

import java.io.IOException;
import java.net.UnknownHostException;

import org.junit.Test;

/**
 * TESTING STRATEGY
 * 
 * 1. Runs WhiteboardServer with arguments
 * 
 * 2. Instantiates the correct number of canvases/palettes in whiteboards
 * 
 * 3. Server accepts incoming connections without error
 * 
 * 4. Correctly interprets messages from the client's drawing controller, 
 *      updates master canvas and sends messages to all client(s) without error
 * 
 * 5. Client and the corresponding output is removed from the server (users) when client disconnects
 */
public class WhiteboardServerTest {

    @Test
    public void testRunMainWithArguments() throws UnknownHostException, IOException, IllegalArgumentException {
        // Run Configurations for WhiteboardServer
        // Enter a flag followed by a value [-host HOST] [-port PORT] [-boards BOARDS]
        // Host: localhost, personal IP address, working IP address, invalid host, invalid IP address
        // Only localhost, personal IP address, and working IP address should work, other will throw UnknownHostException
        // Ports: negative number, 0, 1, number between 1 and 65534, number greater than 65534
        // Only number between 1 and 65534 should work, others will throw IllegalArgumentException
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testConstructWhiteboards() {
        // Run WhiteboardServer
        // Run WhiteboardClient with matching host and port.  
        // When Window GUI opens make a mark on canvas then switch to next canvas. 
        // Client should be able to add or change boards
        boolean SUCCESSFUL = false;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testServeSocketAccept() {
        // Start WhiteboardServer and nothing should happen. 
        // When WhiteboardClient is run with matching host and port then client should be able to run correctly. 
        // If WhiteboardClient is run with mismatching host and port then client will not be able to run correctly.
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testServerCommunication() {
        // LoginUI will keep reappearing if blank, only-spaces, or already-used username inputted
        // Client's username appears on top window
        // Other usernames that are active should appear on the right area
        // Client can see past drawing on board zero when starting up
        // Client can see own draw actions
        // Client can see own erase actions
        // Client can see own fill actions
        // Client can make lines actions
        // Client can make rectangles actions
        // Client can make ovals actions
        // Client can change to other boards
        // Client can see these actions from all other users
        // All changes mentioned above should be visible to all connected clients
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testRemoveClientUponDisconnection() {
        // Client closing window or ending program should not affect other users canvas
        // Other clients can no longer see username of person who disconnected.  
        // Client can log back in later and be able to resume activities
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }

}
