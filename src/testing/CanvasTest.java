package testing;

import static org.junit.Assert.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

import ui.Canvas;
import ui.Palette;

/**
 * Testing Strategy:
 * 
 * We created a MockCanvas class that retains only the drawing functions of Canvas and checks
 * that Canvas is drawing the correct pictures by calling same drawing functions on Canvas and
 * MockCanvas and making sure their drawingBuffers are identical. And we tested the following
 * functions:
 * 
 * 1. Initializing a blank canvas
 * 2. The draw function
 * 3. The erase function
 * 4. The fill function
 * 5. The drawRect function
 * 6. The drawOval function
 * 
 */

public class CanvasTest {
    
    // makes sure Canvas initializes to a blank, white drawing buffer
    @Test
    public void blankTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);     
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }
    
    // makes sure Canvas draws straight lines correctly
    @Test
    public void lineTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);
        
        canvas.draw(1, 2, 8, 9, Color.CYAN.getRGB(), 2);
        mock.draw(1, 2, 8, 9, Color.cyan.getRGB(), 2);
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }
    
    // makes sure Canvas can erase correctly
    @Test
    public void eraseTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);
        
        canvas.draw(1, 6, 8, 9, Color.CYAN.getRGB(), 2);
        mock.draw(1, 6, 8, 9, Color.CYAN.getRGB(), 2);
        
        canvas.erase(1, 2, 8, 9, 2);
        mock.erase(1, 2, 8, 9, 2);
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }
    
    // makes sure Canvas can fill correctly
    @Test
    public void fillTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);
        
        canvas.draw(1, 1, 1, 5, Color.CYAN.getRGB(), 2);
        mock.draw(1, 1, 1, 5, Color.CYAN.getRGB(), 2);
        
        canvas.draw(1, 5, 9, 5, Color.CYAN.getRGB(), 2);
        mock.draw(1, 5, 9, 5, Color.CYAN.getRGB(), 2);
        
        canvas.draw(9, 5, 9, 1, Color.CYAN.getRGB(), 2);
        mock.draw(9, 5, 9, 1, Color.CYAN.getRGB(), 2);
        
        canvas.draw(9, 1, 1, 1, Color.CYAN.getRGB(), 2);
        mock.draw(9, 1, 1, 1, Color.CYAN.getRGB(), 2);
        
        canvas.fill(3, 3, Color.PINK.getRGB());
        mock.fill(3, 3, Color.PINK.getRGB());
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }

    // makes sure Canvas can draw rectangles correctly
    @Test
    public void drawRectTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);
        
        canvas.drawRect(1, 2, 8, 9, Color.CYAN.getRGB(), 2);
        mock.drawRect(1, 2, 8, 9, Color.CYAN.getRGB(), 2);
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }
    
    // makes sure Canvas can draw ovals correctly
    @Test
    public void drawOvalTest() {
        int width = 10;
        int height = 10;
        Palette palette = new Palette(width, height);
        Canvas canvas = new Canvas(width, height, palette, "");
        MockCanvas mock = new MockCanvas(width, height);
        
        canvas.drawOval(1, 2, 8, 9, Color.CYAN.getRGB(), 2);
        mock.drawOval(1, 2, 8, 9, Color.CYAN.getRGB(), 2);
        
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                assertEquals(true, mock.drawingBuffer.getRGB(x, y) == canvas.getImageRGB(x, y));
            }
        }
    }
    
    // mock canvas class
    private class MockCanvas {
        
        BufferedImage drawingBuffer;
        
        public MockCanvas(int width, int height) {
            drawingBuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            
            g2.setColor(Color.WHITE);
            g2.fillRect(0, 0, width, height);
        }
        
        public void draw(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            
            g2.setStroke(new BasicStroke(strokeWidth));
            g2.setColor(new Color(rgb));
            g2.drawLine(x1, y1, x2, y2);
        }
        
        public void erase(int x1, int y1, int x2, int y2, int strokeWidth) {
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            
            g2.setStroke(new BasicStroke(strokeWidth));
            g2.setColor(Color.WHITE);
            g2.drawLine(x1, y1, x2, y2);
        }
        
        public void fill(int x, int y, int rgb) {
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();

            g2.setColor(new Color(rgb));
            int fillRGB = g2.getColor().getRGB();
            int regionRGB = drawingBuffer.getRGB(x, y);

            if (regionRGB == rgb) return;

            Queue<int[]> pixelQueue = new LinkedList<int[]>();
            pixelQueue.add(new int[] {x, y});        
            int[] pixel;
            int nx, ny;

            while (!pixelQueue.isEmpty()) {
                pixel = pixelQueue.poll();
                nx = pixel[0];
                ny = pixel[1];
                if (drawingBuffer.getRGB(nx, ny) == regionRGB) {
                    drawingBuffer.setRGB(nx, ny, fillRGB);

                    for (int[] neighbor : getNeighborPixels(nx, ny)) {
                        if (drawingBuffer.getRGB(neighbor[0], neighbor[1]) != fillRGB) {
                            pixelQueue.add(new int[] {neighbor[0], neighbor[1]});
                        }
                    }
                }
            }
        }
        
        private int[][] getNeighborPixels(int x, int y) {
            return new int[][] {
                  {Math.min(x + 1, drawingBuffer.getWidth() - 1), y},  //right neighbor
                  {Math.max(x - 1, 0), y},               //left neighbor
                  {x, Math.min(y + 1, drawingBuffer.getHeight() - 1)}, //up neighbor
                  {x, Math.max(y - 1, 0)}                //down neighbor
            };
        }
    
        public void drawRect(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            g2.setColor(new Color(rgb));
            g2.setStroke(new BasicStroke(strokeWidth));
            
            g2.drawRect(
                    (x1 < x2) ? x1 : x2,
                    (y1 < y2) ? y1 : y2,
                    Math.abs(x1 - x2),
                    Math.abs(y1 - y2)
                    );
        }
        
        public void drawOval(int x1, int y1, int x2, int y2, int rgb, int strokeWidth) {
            Graphics2D g2 = (Graphics2D) drawingBuffer.getGraphics();
            
            g2.setColor(new Color(rgb));
            g2.setStroke(new BasicStroke(strokeWidth));
            
            g2.drawOval(
                    (x1 < x2) ? x1 : x2,
                    (y1 < y2) ? y1 : y2,
                    Math.abs(x1 - x2),
                    Math.abs(y1 - y2));
        }
    }
}
        
    
