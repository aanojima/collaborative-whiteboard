package testing;

import org.junit.Test;

/**
 * TESTING STRATEGY
 * 
 * 1. Instantiates the client when client logs in. A client who enters
 *    a blank, space-only, or username that is already taken 
 *    will be asked to re-enter a new username). 
 * 
 * 2. Connect to the server without error, handles, and correctly interprets the messages from the server
 * 
 * 3. Supports host and port arguments when running.
 *   
 */
public class WhiteboardClientTest {

    @Test
    public void testWhiteboardClientConstructor() {
        // LoginUI will keep opening if blank, all-spaces, or already-taken username submitted. 
        // Window (Canvas/Palette/UsersUI) opens once login is successful. 
        // Window Frame has username on top. 
        // UsersUI has list of users connected. 
        // Palette is functional - clicks, toggles,  and sliders work. 
        // Canvas has a drawingController (can listen to mouse events - draw)
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testConnectionAndHandleResponse() {
        // Client can see past drawing on board zero when starting up
        // Client can see own draw actions
        // Client can see own erase actions
        // Client can see own fill actions
        // Client can make lines actions
        // Client can make rectangles actions
        // Client can make ovals actions
        // Client can change to other boards
        // Client can see these actions from all other users
        // All changes mentioned above should be visible to all connected clients
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
    @Test
    public void testMain() {
        // Run Configurations for WhiteboardClient
        // Enter a flag followed by a value [-host HOST] [-port PORT]
        // Host: localhost, personal IP address, working IP address, invalid host, invalid IP address
        // Only localhost, personal IP address, and working IP address should work, other will throw UnknownHostException
        // Ports: negative number, 0, 1, number between 1 and 65534, number greater than 65534
        // Only number between 1 and 65534 should work, others will throw IllegalArgumentException
        // If host and port did not start WhiteboardServer, then should throw an UnkownHostException. 
        boolean SUCCESSFUL = true;
        assert SUCCESSFUL;
    }
    
}
